const keystone = require('keystone');
const Polyglot = require('node-polyglot');

const langRegex = /^\/([A-Z]{2})([\/\?].*)?$/i;

/**
 * Set locale for request based on URL param
 *
 * @param {Object} req Request object
 * @param {Object} res Response object
 * @param {Function} next Callback
 */
exports.setLocale = (req, res, next) => {
  const match = req.url.match(langRegex);
  let locale = 'en';

  if (match) {
    locale = match[1];

    req.url = match[2] || '/';
  } else {
    // locale = req.locale; TODO: reenable for lang detect
  }

  res.locals.locale = locale;
  res.locals.url = req.url;
  res.locals.baseUrl = `https://www.dokaphotos.com${req.url}`;
  res.locals.polyglot = new Polyglot({
    locale,
    phrases: require(`../locales/${locale}.json`),
  });

  next();
};

/**
  Initialises the standard view locals

  The included layout depends on the navLinks array to generate
  the navigation in the header, you may wish to change this array
  or replace it with your own templates / logic.
*/
exports.initLocals = (req, res, next) => {
  res.locals.user = req.user;

  keystone.list('Page')
    .model
    .find()
    .exec()
    .then((pages) => {
      res.locals.navLinks = pages.map((page) => ({
        href: `/${page.slug === 'home' ? '' : page.slug}`,
        key: page.slug,
        label: page.title[res.locals.locale],
      }));

      next();
    });
};


/**
 * Fetches and clears the flashMessages before a view is rendered
 */
exports.flashMessages = (req, res, next) => {
  const flashMessages = {
    info: req.flash('info'),
    success: req.flash('success'),
    warning: req.flash('warning'),
    error: req.flash('error'),
  };

  res.locals.messages = Object.keys(flashMessages).some((key) => flashMessages[key].length)
    ? flashMessages
    : false;

  next();
};


/**
  Prevents people from accessing protected pages when they're not signed in
 */
exports.requireUser = (req, res, next) => {
  if (!req.user) {
    req.flash('error', 'Please sign in to access this page.');
    res.redirect('/keystone/signin');
  } else {
    next();
  }
};

exports.getSocialLinks = (req, res, next) =>
  keystone.list('Social')
    .model
    .find()
    .exec()
    .then((data) => {
      res.locals.social = data;

      next();
    });
