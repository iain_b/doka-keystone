const keystone = require('keystone');

module.exports = (req, res) => {
  const view = new keystone.View(req, res);

  res.locals.section = 'about';

  view.on('init', (next) => {
    keystone.list('Page')
      .model
      .findOne({ slug: 'about' })
      .exec()
      .then((page) => {
        res.locals.data = page;
        res.locals.title = page.title;
        res.locals.description = page.description;
        res.locals.image = page.banner;

        next();
      })
      .catch((err) => {
        console.error(err);

        next(err);
      });
  });

  view.render('about');
};
