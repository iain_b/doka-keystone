const keystone = require('keystone');

const Page = keystone.list('Page').model;
const Post = keystone.list('Post').model;

module.exports = (req, res) => {
  const view = new keystone.View(req, res);
  const { locals } = res;

  locals.section = 'home';

  view.on(
    'init',
    (next) =>
      Page.findOne({ slug: 'home' })
        .exec()
        .then((page) => {
          locals.content = page;
          locals.title = page.title;
          locals.description = page.description;
          locals.image = page.banner;

          next();
        })
        .catch((err) => {
          next(err);
        }),
  );

  view.on(
    'init',
    (next) =>
      Post.find()
        .where('state', 'published')
        .sort('-publishedDate')
        .populate('author')
        .limit(1)
        .exec()
        .then((post) => {
          [locals.latestPost] = post;

          next();
        })
        .catch((err) => {
          console.log('error', err);

          next(err);
        }),
  );

  view.render('index');
};
