const keystone = require('keystone');

module.exports = (req, res) => {
  const view = new keystone.View(req, res);

  res.locals.section = 'blog';
  res.locals.filters = { post: req.params.post };
  res.locals.data = { posts: [] };

  view.on('init', (next) => {
    keystone.list('Post').model
      .findOne({
        state: 'published',
        slug: res.locals.filters.post,
      })
      .populate('author categories')
      .exec()
      .then((post) => {
        res.locals.data.post = post; // TODO: overly nested
        res.locals.title = post.title;
        res.locals.description = post.summary;
        res.locals.image = post.image;

        next();
      })
      .catch((err) => {
        console.error(err);

        next(err);
      });
  });

  view.on('init', (next) => {
    keystone.list('Post')
      .model
      .find()
      .where('state', 'published')
      .sort('-publishedDate')
      .populate('author')
      .limit(4)
      .exec()
      .then((posts) => {
        res.locals.data.posts = posts;

        next();
      })
      .catch((err) => {
        console.error(err);

        next(err);
      });
  });

  view.render('post');
};
