const keystone = require('keystone');

module.exports = (req, res) => {
  const view = new keystone.View(req, res);

  res.locals.section = 'blog';
  res.locals.filters = { category: req.params.category };
  res.locals.data = { posts: [], categories: [] };

  view.on('init', (next) => {
    keystone.list('Page')
      .model
      .findOne({ slug: 'blog' })
      .exec()
      .then((page) => {
        res.locals.page = page;
        res.locals.title = page.title;
        res.locals.description = page.description;
        res.locals.image = page.banner;

        next();
      })
      .catch((err) => {
        console.error(err);

        next(err);
      });
  });

  // Load all categories
  view.on('init', (next) => {
    keystone.list('PostCategory')
      .model
      .find()
      .sort('name')
      .exec()
      .then((results) => {
        res.locals.data.categories = results;

        Promise.all(results.map((category) =>
          keystone.list('Post')
            .model
            .count()
            .where('categories')
            .in([category.id])))
          .then((counts) => {
            counts.forEach((count, i) => {
              res.locals.data.categories[i].postCount = count;
            });

            next();
          });
      });
  });

  // Load the current category filter
  view.on('init', (next) => {
    if (req.params.category) {
      keystone.list('PostCategory')
        .model
        .findOne({ key: res.locals.filters.category })
        .exec()
        .then((result) => {
          res.locals.data.category = result;

          next();
        })
        .catch((err) => {
          console.error(err);

          next(err);
        });
    } else {
      next();
    }
  });

  // Load the posts
  view.on('init', (next) => {
    const query = keystone.list('Post') // TODO: make promise
      .paginate({
        page: req.query.page || 1,
        perPage: 10,
        maxPages: 10,
        filters: { state: 'published' },
      })
      .sort('-publishedDate')
      .populate('author categories');

    if (res.locals.data.category) {
      query.where('categories').in([res.locals.data.category]);
    }

    query.exec((err, results) => {
      if (err) {
        next(err);
      }

      res.locals.data.posts = results;

      next();
    });
  });

  view.render('blog');
};
