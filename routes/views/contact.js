const keystone = require('keystone');

const Enquiry = keystone.list('Enquiry').model;

module.exports = (req, res) => {
  const view = new keystone.View(req, res);
  const { locals } = res;

  locals.section = 'contact';
  locals.formData = req.body || {};
  locals.validationErrors = {};
  locals.enquirySubmitted = false;

  view.on('init', (next) => {
    keystone.list('Page')
      .model
      .findOne({ slug: 'contact' })
      .exec()
      .then((page) => {
        res.locals.page = page;
        res.locals.title = page.title;
        res.locals.description = page.description;
        res.locals.image = page.banner;

        next();
      })
      .catch((err) => {
        console.error(err);

        next(err);
      });
  });

  // On POST requests, add the Enquiry item to the database
  view.on('post', { action: 'contact' }, (next) => {
    const newEnquiry = new Enquiry();
    const updater = newEnquiry.getUpdateHandler(req);

    updater.process(req.body, {
      flashErrors: true,
      fields: 'name, email, subject, message',
      errorMessage: 'contact.error_message',
    }, (err) => {
      if (err) {
        locals.validationErrors = err.errors;
      } else {
        locals.enquirySubmitted = true;
      }

      next();
    });
  });

  view.render('contact');
};
