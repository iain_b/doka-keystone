const keystone = require('keystone');

module.exports = (req, res) => {
  const view = new keystone.View(req, res);

  res.locals.section = 'gallery';

  // load page
  view.on('init', (next) => {
    keystone.list('Page')
      .model
      .findOne({ slug: 'gallery' })
      .exec()
      .then((page) => {
        res.locals.page = page;
        res.locals.title = page.title;
        res.locals.description = page.description;
        res.locals.image = page.banner;

        next();
      })
      .catch((err) => {
        console.error(err);

        next(err);
      });
  });

  // load galleries
  view.on('init', (next) => {
    const galleryLookup = keystone.list('Gallery')
      .model
      .find()
      .populate('items')
      .exec();

    galleryLookup.then((galleries) => {
      res.locals.galleries = galleries.map((gallery) => {
        [gallery.firstItem] = gallery.items;
        gallery.items = gallery.items.slice(1, gallery.items.length);

        return gallery;
      });

      next();
    });
  });

  view.render('gallery');
};
