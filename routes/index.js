const keystone = require('keystone');
const locale = require('locale');
const sitemap = require('keystone-express-sitemap');
const middleware = require('./middleware');

const importRoutes = keystone.importer(__dirname);

module.exports = {
  init() {
    keystone.pre('routes', locale(['en', 'fr', 'nl'], 'en'));
    keystone.pre('routes', middleware.setLocale);
    keystone.pre('routes', middleware.initLocals);
    keystone.pre('render', middleware.flashMessages);
    keystone.pre('render', middleware.getSocialLinks);
  },

  routes(app) {
    const routes = { views: importRoutes('./views') };

    app.set('x-powered-by', false);

    app.get('/sitemap.xml', (req, res) =>
			sitemap.create(keystone, req, res)
    );

    app.get('/', routes.views.index);
    app.get('/blog/:category?', routes.views.blog);
    app.get('/blog/post/:post', routes.views.post);
    app.get('/gallery', routes.views.gallery);
    app.get('/about', routes.views.about);
    app.get('/pricing', routes.views.pricing);
    app.all('/contact', routes.views.contact);

    // NOTE: To protect a route so that only admins can see it, use the requireUser middleware:
    // app.get('/protected', middleware.requireUser, routes.views.protected);
  },
};
