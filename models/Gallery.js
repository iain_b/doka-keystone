const keystone = require('keystone');
const i18nFields = require('./i18n/fields');

const { Types } = keystone.Field;

const Gallery = new keystone.List('Gallery', {
  autokey: { from: 'name.en', path: 'key', unique: true },
  map: { name: 'name.en' },
});

Gallery.add(
  { name: i18nFields.string(false, true) },
  { description: i18nFields.wysiwyg() },
  { items: { type: Types.Relationship, ref: 'GalleryItem', many: true } },
);

Gallery.defaultColumns = 'name.en';
Gallery.register();
