const keystone = require('keystone');
const Email = require('keystone-email');

const { Types } = keystone.Field;

const Enquiry = new keystone.List('Enquiry', {
  nocreate: true,
  noedit: true,
});

Enquiry.add({
  name: { type: Types.Name, required: true },
  email: { type: Types.Email, required: true },
  subject: { type: String, required: true },
  message: { type: Types.Markdown, required: true },
  createdAt: { type: Date, default: Date.now },
});

Enquiry.schema.pre('save', function(next) {
  this.wasNew = this.isNew;
  next();
});

Enquiry.schema.post('save', function() {
  if (this.wasNew) {
    this.sendNotificationEmail();
  }
});

Enquiry.schema.methods.sendNotificationEmail = function(callback) {
  const next = typeof callback === 'function'
    ? callback
    : (err) => {
      if (err) {
        console.error('There was an error sending the notification email:', err);
      }
    };

  if (!process.env.MAILGUN_API_KEY || !process.env.MAILGUN_DOMAIN) {
    console.log('Unable to send email - no mailgun credentials provided');

    return next(new Error('could not find mailgun credentials'));
  }

  const enquiry = this;
  const brand = keystone.get('brand');

  return keystone.list('User')
    .model
    .find()
    .where('isAdmin', true)
    .exec()
    .then((admins) => {
      return new Email(
        './templates/emails/enquiry-notification.pug',
        { transport: 'mailgun' },
      ).send(
        { brand, enquiry },
        {
          from: { name: 'Doka Photos', email: 'noreply@dokaphotos.com' },
          'o:tracking': false,
          subject: enquiry.subject,
          to: admins.map(a => ({ email: a.email, name: `${a.name.first} ${a.name.last}`})),
        },
        (err, result) => {
          if (err) {
            console.error('🤕 Mailgun test failed with error:\n', err);
          } else {
            console.log('📬 Successfully sent Mailgun test with result:\n', result);
          }
        }
      );
    })
    .catch((err) => {
      console.error(err);
    });
};

Enquiry.defaultSort = '-createdAt';
Enquiry.defaultColumns = 'name, email, createdAt';
Enquiry.register();
