const keystone = require('keystone');

const locales = ['en', 'fr', 'nl']; // TODO: get from i18n

const types = (type) => {
  switch (type) {
    case 'string':
      return { type: String };
    case 'wysiwyg':
      return { type: keystone.Field.Types.Html, wysiwyg: true, height: 150 };
    default:
      throw new Error('invalid field type specified');
  }
};

const Fields = {
  build: (type, template, required) => {
    const fields = {};

    locales.forEach((locale, i) => {
      fields[locale] = types(type);

      if (template) {
        fields[locale].dependsOn = { template };
      }

      if (i === 0 && required) {
        fields[locale].required = true;
        fields[locale].initial = true;
      }
    });

    return fields;
  },
};

module.exports = new Proxy(Fields, {
  get(target, key) {
    return (template = false, required = false) => Fields.build(key, template, required);
  },
});
