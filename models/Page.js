const keystone = require('keystone');
const i18nFields = require('./i18n/fields');

const { Types } = keystone.Field;

const Page = new keystone.List('Page', {
  map: { name: 'title.en' },
  autokey: { path: 'slug', from: 'title.en', unique: true },
});

Page.add(
  { title: i18nFields.string(false, true) },
  { description: i18nFields.string() },
  { banner: { type: Types.CloudinaryImage } },
  { template: { type: Types.Select, options: 'home,gallery,blog,about,contact' } },

  // home page options
  { heading: 'Intro', dependsOn: { template: 'home' } },
  {
    intro: {
      title: i18nFields.string('home'),
      body: i18nFields.wysiwyg('home'),
      button: i18nFields.string('home'),
    },
  },
  { heading: 'Services', dependsOn: { template: 'home' } },
  {
    services: {
      title: i18nFields.string('home'),
      image1: {
        before: { type: Types.CloudinaryImage, dependsOn: { template: 'home' } },
        after: { type: Types.CloudinaryImage, dependsOn: { template: 'home' } },
        title: i18nFields.string('home'),
        body: i18nFields.wysiwyg('home'),
      },
      image2: {
        before: { type: Types.CloudinaryImage, dependsOn: { template: 'home' } },
        after: { type: Types.CloudinaryImage, dependsOn: { template: 'home' } },
        title: i18nFields.string('home'),
        body: i18nFields.wysiwyg('home'),
      },
      image3: {
        before: { type: Types.CloudinaryImage, dependsOn: { template: 'home' } },
        after: { type: Types.CloudinaryImage, dependsOn: { template: 'home' } },
        title: i18nFields.string('home'),
        body: i18nFields.wysiwyg('home'),
      },
      help: i18nFields.string('home'),
      button: i18nFields.string('home'),
      message: i18nFields.wysiwyg('home'),
    },
  },
  { heading: 'Latest Post', dependsOn: { template: 'home' } },
  { latestPost: i18nFields.string('home') },
  { morePosts: i18nFields.string('home') },

  // gallery page
  { intro: i18nFields.wysiwyg('gallery') },
  { outro: i18nFields.wysiwyg('gallery') },
  { button: i18nFields.string('gallery') },

  // about page
  { content: i18nFields.wysiwyg('about') },

  // contact page
  { info: i18nFields.wysiwyg('contact') },
);

Page.defaultColumns = 'title.en';
Page.register();
