const keystone = require('keystone');

const Social = new keystone.List('Social', {
  autokey: { path: 'slug', from: 'name', unique: true },
  map: { name: 'name' },
  sortable: true,
});

Social.add(
  { name: { type: String, required: true } },
  { url: { type: String } },
  { icon: { type: String } },
);

Social.defaultColumns = 'name, url';
Social.register();
