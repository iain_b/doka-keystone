const keystone = require('keystone');
const i18nFields = require('./i18n/fields');

const { Types } = keystone.Field;

const GalleryItem = new keystone.List('GalleryItem', {
  autokey: { from: 'title.en', path: 'key', unique: true },
  map: { name: 'title.en' },
  sortable: true, // how to sort within things
});

GalleryItem.add(
  { title: i18nFields.string(false, true) },
  { slider: Types.Boolean },
  { first_image: Types.CloudinaryImage },
  { second_image: { type: Types.CloudinaryImage, dependsOn: { slider: true } } },
  { description: i18nFields.wysiwyg() },
);

GalleryItem.relationship({
  path: 'galleries',
  ref: 'Gallery',
  refPath: 'items',
});

GalleryItem.register();
