const keystone = require('keystone');
const i18nFields = require('./i18n/fields');

const { Types } = keystone.Field;

const Post = new keystone.List('Post', {
  map: { name: 'title.en' },
  autokey: { path: 'slug', from: 'title.en', unique: true },
});

Post.add(
  { title: i18nFields.string() },
  {
    state: {
      type: Types.Select,
      options: 'draft, published, archived',
      default: 'draft',
      index: true,
    },
  },
  { author: { type: Types.Relationship, ref: 'User', index: true } },
  { publishedDate: { type: Types.Date, index: true, dependsOn: { state: 'published' } } },
  { image: { type: Types.CloudinaryImage } },
  { summary: i18nFields.wysiwyg() },
  { extended: i18nFields.wysiwyg() },
  { categories: { type: Types.Relationship, ref: 'PostCategory', many: true } },
);

Post.schema
  .virtual('content.full')
  .get(() => this.content.extended || this.content.brief);

Post.defaultColumns = 'title.en, state|20%, author|20%, publishedDate|20%';
Post.register();
