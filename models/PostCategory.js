const keystone = require('keystone');
const i18nFields = require('./i18n/fields');

const PostCategory = new keystone.List('PostCategory', {
  autokey: { from: 'name.en', path: 'key', unique: true },
  map: { name: 'name.en' },
});

PostCategory.add({ name: i18nFields.string(false, true) });

PostCategory.relationship({ ref: 'Post', path: 'posts', refPath: 'categories' });

PostCategory.register();
