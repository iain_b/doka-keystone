const test = require('tape');
const helpers = require('../../templates/helpers/keystone');

test('isAdminEditorCSS()', (assert) => {
  assert.equal(helpers.isAdminEditorCSS().string, '', 'does nothing if no user passed');
  assert.equal(
    helpers.isAdminEditorCSS({ isAdmin: false }).string,
    '',
    'does nothing if user not admin',
  );
  assert.equal(
    helpers.isAdminEditorCSS({ isAdmin: true }).string,
    '<link href="/keystone/styles/content/editor.min.css" rel="stylesheet">',
    'returns correct string if admin',
  );

  assert.end();
});

test('isAdminEditorJS()', (assert) => {
  assert.equal(helpers.isAdminEditorJS().string, '', 'does nothing if no user passed');
  assert.equal(
    helpers.isAdminEditorJS({ isAdmin: false }).string,
    '',
    'does nothing if user not admin',
  );
  assert.equal(
    helpers.isAdminEditorJS({ isAdmin: true }).string,
    '<script src="/keystone/js/content/editor.js"></script>',
    'returns correct string if admin',
  );

  assert.end();
});
