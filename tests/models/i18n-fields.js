const test = require('tape');
const i18nFields = require('../../models/i18n/fields');

test('the magic method', (assert) => {
  const templateField = i18nFields.string('home');

  assert.equal(
    templateField.en.dependsOn.template,
    'home',
    'template field includes dependsOn clause',
  );

  const nonTemplate = i18nFields.wysiwyg();

  assert.equal(
    nonTemplate.en.dependsOn,
    undefined,
    'non-template field does not include dependsOn',
  );

  const required = i18nFields.string(false, true);

  assert.equal(
    required.en.required,
    true,
    'required field is included when specified',
  );
  assert.equal(
    required.en.initial,
    true,
    'initial field is included when required specified',
  );
  assert.equal(
    required.fr.required,
    undefined,
    'required field not included with other languages',
  );
  assert.equal(
    required.fr.initial,
    undefined,
    'intial field not included with other languages',
  );

  const requiredWithTemplate = i18nFields.wysiwyg('home', true);

  assert.equal(
    requiredWithTemplate.en.required,
    true,
    'required field is included when specified',
  );
  assert.equal(
    requiredWithTemplate.en.initial,
    true,
    'initial field is included when required specified',
  );

  assert.throws(
    () => i18nFields.fart(),
    'invalid field type specified',
  );

  assert.end();
});
