require('dotenv').config();
const keystone = require('keystone');
const viewEngine = require('./templates/engine');
const routes = require('./routes');

keystone.init({
  auth: true,
  'auto update': true,
  brand: 'Doka Photos',
  'cloudinary secure': true,
  'custom engine': viewEngine(),
  emails: 'templates/emails',
  favicon: 'public/favicon.png',
  name: 'Doka Photos',
  session: true,
  'session store': 'mongo',
  static: 'public',
  'user model': 'User',
  'view engine': '.hbs',
  views: 'templates/views',
  'wysiwyg additional options': {
    external_plugins: { 'uploadimage': '/js/uploadimage/plugin.min.js' },
  },
  'wysiwyg cloudinary images': true,
});

keystone.import('models');

keystone.set('locals', {
  env: keystone.get('env'),
  utils: keystone.utils,
  editable: keystone.content.editable,
});

routes.init();

keystone.set('routes', routes.routes);

// Configure the navigation bar in Keystone's Admin UI
keystone.set('nav', {
  pages: 'pages',
  posts: ['posts', 'post-categories'],
  galleries: ['galleries', 'gallery-items'],
  enquiries: 'enquiries',
  users: 'users',
});

if (!process.env.MAILGUN_API_KEY || !process.env.MAILGUN_DOMAIN) {
  console.log('----------------------------------------'
  + '\nWARNING: MISSING MAILGUN CREDENTIALS'
  + '\n----------------------------------------'
  + '\nYou have opted into email sending but have not provided'
  + '\nmailgun credentials. Attempts to send will fail.'
  + '\n\nCreate a mailgun account and add the credentials to the .env file to'
  + '\nset up your mailgun integration');
}

keystone.start();
