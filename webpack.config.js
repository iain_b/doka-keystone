const ExtractTextPlugin = require('extract-text-webpack-plugin');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');

const extractSass = new ExtractTextPlugin({
  filename: `./public/styles/site.css`,
});

module.exports = {
  entry: {
    filename: './assets/js/site.js'
  },
  module: {
    rules: [{
      test: /\.scss$/,
      loader: extractSass.extract({
        use: [
          { loader: 'css-loader', options: { minimize: true } },
          { loader: 'sass-loader' }
        ],
      }),
    }, {
      test: /\.css$/,
      loader: 'style-loader!css-loader!postcss-loader',
      options: {
        minimize: true,
        plugins: function () {
          return [
            require('autoprefixer')
          ];
        }
      }
    }]
  },
  output: {
    filename: './public/js/site.js'
  },
  plugins: [
    extractSass,
    new UglifyJsPlugin({
      uglifyOptions: {
        ecma: 6,
      }
    }),
  ],
};
