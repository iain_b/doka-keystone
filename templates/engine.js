const handlebars = require('express-handlebars');
const generalHelpers = require('./helpers/general');
const i18nHelpers = require('./helpers/i18n');
const keystoneHelpers = require('./helpers/keystone');

module.exports = () => {
  const helpers = Object.assign({}, generalHelpers, i18nHelpers, keystoneHelpers);

  return handlebars.create({
    defaultLayout: 'default',
    extname: '.hbs',
    helpers,
    layoutsDir: `${__dirname}/views/layouts`,
    partialsDir: `${__dirname}/views/partials`,
  }).engine;
};
