const distanceInWordsToNow = require('date-fns/distance_in_words_to_now');
const formatDate = require('date-fns/format');
const en = require('date-fns/locale/en');
const fr = require('date-fns/locale/fr');
const nl = require('date-fns/locale/nl');
const hbs = require('handlebars');

const dateLocales = { en, fr, nl };
const locales = ['en', 'fr', 'nl'];

const i18nHelpers = {
  __(key) {
    return this.polyglot.t(key);
  },

  /**
   * Get property value for current locale
   *
   * @param {*} prop Property
   * @returns Value for locale
   */
  t(prop) {
    return prop[this.locale];
  },

  /**
   * Generate hreflang alternate tags
   *
   * @param {string} url Relative url
   * @returns Tag list
   */
  hreflang(url) {
    return locales.filter(l => l !== this.locale)
      .map((loc) => `<link rel="alternate" hreflang="${loc}" href="http://www.dokaphotos.com/${loc}${url}">`)
      .join('\n');
  },

  /**
   * Generate og:locale:alternate tags
   *
   * @returns Tag list
   */
  ogLocale() {
    return locales.filter(l => l !== this.locale)
      .map((loc) => `<meta property="og:locale:alternate" content="${loc}">`)
      .join('\n');
  },

  /**
   * Localised page url
   *
   * @param {number} pageNumber Page url to generate
   * @returns {string} Page url
   */
  pageUrl(pageNumber) {
    return `/${this.locale}/blog?page=${pageNumber}`;
  },

  /**
   * Localised post url
   *
   * @param {string} postSlug Post slug
   * @returns {string} Post url
   */
  postUrl(postSlug) {
    return (`/${this.locale}/blog/post/${postSlug}`);
  },

  /**
   * Localised category url
   *
   * @param {string} categorySlug Category slug
   * @returns {string} Category url
   */
  categoryUrl(categorySlug) {
    return (`/${this.locale}/blog/${categorySlug}`);
  },

  /**
   * Return a localised date string
   *
   * @param {object} context
   * @param {object} options
   *
   * @returns {string} Date string
   */
  date(context, options) {
    const dateOptions = options || context;
    const date = context ? (context.hash ? new Date() : context) : new Date();
    const locale = { locale: dateLocales[this.locale] };

    if (dateOptions.hash.timeago) {
      const dateString = distanceInWordsToNow(date, locale);

      return this.polyglot.t('date.ago', { date: `${dateString}` });
    }

    return formatDate(date, dateOptions.hash.format || 'MMM Do, YYYY', locale);
  },

  // special helper to ensure that we always have a valid page url set even if
  // the link is disabled, will default to page 1
  paginationPrev(previousPage) {
    return i18nHelpers.pageUrl(previousPage === false ? 1 : previousPage);
  },

  // special helper to ensure that we always have a valid next page url set
  // even if the link is disabled, will default to totalPages
  paginationNext(nextPage, totalPages) {
    return i18nHelpers.pageUrl(nextPage === false ? totalPages : nextPage);
  },

  flashMessages(messages) {
    let output = '';

    for (let i = 0; i < messages.length; ++i) {
      if (messages[i].title) {
        output += `<h4>${this.polyglot.t(messages[i].title)}</h4>`;
      }

      if (messages[i].detail) {
        output += `<p>${messages[i].detail}</p>`;
      }

      if (messages[i].list) {
        output += '<ul>';

        for (let j = 0; j < messages[i].list.length; ++j) {
          output += `<li>${messages[i].list[j]}</li>`;
        }

        output += '</ul>';
      }
    }

    return new hbs.SafeString(output);
  },
};

module.exports = i18nHelpers;
