const hbs = require('handlebars');

const GeneralHelpers = {
  get: (content, prop) => content ? content[prop] : null,

  log: (...args) => console.log(args[0]), // TODO: better logging

  ifThenElse: (condition, a, b) => condition ? a : b,

  ifeq: (a, b) => a == b,

  encode(string, options) {
    const uri = string || options;

    return new hbs.SafeString(encodeURIComponent(uri));
  },
};

module.exports = GeneralHelpers;
