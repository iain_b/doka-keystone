const keystone = require('keystone');
const cloudinary = require('cloudinary');
const hbs = require('handlebars');

const KeystoneHelpers = {
  isAdminEditorCSS(user) {
    const output = typeof user !== 'undefined' && user.isAdmin
      ? '<link href="/keystone/styles/content/editor.min.css" rel="stylesheet">'
      : '';

    return new hbs.SafeString(output);
  },

  isAdminEditorJS(user) {
    const output = typeof user !== 'undefined' && user.isAdmin
      ? '<script src="/keystone/js/content/editor.js"></script>'
      : '';

    return new hbs.SafeString(output);
  },

  // Used to generate the link for the admin edit post button
  adminEditableUrl(user, options) {
    return keystone.app.locals.editable(user, {
      list: 'Post',
      id: options,
    });
  },

  cloudinaryUrl(context, options) {
    if (!context) {
      return null;
    }

    const urlOptions = !options && Object.prototype.hasOwnProperty.call(context, 'hash')
      ? context
      : options;

    const urlContext = Object.prototype.hasOwnProperty.call(context, 'hash')
      ? this
      : context;

    if (urlContext && urlContext.public_id) {
      const imageName = urlContext.public_id.concat('.', urlContext.format);

      urlOptions.hash.secure = keystone.get('cloudinary secure') || false;

      return cloudinary.url(imageName, urlOptions.hash);
    }

    return null;
  },

  // ### Content Url Helpers
  // KeystoneJS url handling so that the routes are in one place for easier
  // editing.  Should look at Django/Ghost which has an object layer to access
  // the routes by keynames to reduce the maintenance of changing urls

  /*
  * expecting the data.posts context or an object literal that has `previous` and `next` properties
  * ifBlock helpers in hbs - http://stackoverflow.com/questions/8554517/handlerbars-js-using-an-helper-function-in-a-if-statement
  * */
  ifHasPagination(postContext, options) {
    // if implementor fails to scope properly or has an empty data set
    // better to display else block than throw an exception for undefined
    if (typeof postContext === 'undefined') {
      return options.inverse(this);
    }

    if (postContext.next || postContext.previous) {
      return options.fn(this);
    }

    return options.inverse(this);
  },
};

module.exports = KeystoneHelpers;
