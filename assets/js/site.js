/* eslint-env browser */
/* eslint-disable no-new */

const $ = jQuery = require('jquery');
require('lazysizes');
require('lazysizes/plugins/respimg/ls.respimg');
require('lazysizes/plugins/parent-fit/ls.parent-fit');
require('lazysizes/plugins/bgset/ls.bgset');
require('../sass/site.scss');
const Cocoen = require('cocoen');
require('featherlight/release/featherlight.min.js');

$.featherlight.defaults.afterOpen = function(event) {
  var content = this.$instance.find('.cocoen')[0];

  setTimeout(function() {
    new Cocoen(content);
  }, 100);
};

function initFloatingHeader(header) {
  var progressBar = document.querySelector('progress');
  var title = document.querySelector('h1.title');

  var lastScrollY = window.scrollY;
  var lastWindowHeight = window.innerHeight;
  var lastDocumentHeight = $(document).height();
  var ticking = false;

  function requestTick() {
    if (!ticking) {
      requestAnimationFrame(update);
    }

    ticking = true;
  }

  function update() {
    var trigger = title.getBoundingClientRect().top + window.scrollY;
    var triggerOffset = title.offsetHeight + 35;
    var progressMax = lastDocumentHeight - lastWindowHeight;

    // show/hide floating header
    if (lastScrollY >= trigger + triggerOffset) {
      header.classList.add('floating-active');
    } else {
      header.classList.remove('floating-active');
    }

    progressBar.setAttribute('max', progressMax);
    progressBar.setAttribute('value', lastScrollY);

    ticking = false;
  }

  window.addEventListener('scroll', function () {
    lastScrollY = window.scrollY;
    requestTick();
  }, { passive: true });
  window.addEventListener('resize', function () {
    lastWindowHeight = window.innerHeight;
    lastDocumentHeight = $(document).height();
    requestTick();
  }, false);

  $('.floating-header-share-fb').on('click', function() {
    window.open(this.href, 'share-facebook', 'width=580,height=296');
    return false;
  });

  $('.floating-header-share-tw').on('click', function() {
    window.open(this.href, 'share-twitter', 'width=550,height=235');
    return false;
  });

  update();
}

$(document).ready(function () {
  var header = document.querySelector('.floating-header');

  if (header) {
    initFloatingHeader(header);
  }

  $('.navbar-burger').on('click', function () {
    $(this).toggleClass('is-active');
    $('.navbar-menu').toggleClass('is-active');
  });

  $('.navbar-link').on('click', (el) => {
    $(el.target).parent().toggleClass('is-active');

    return false;
  });

  $('.blog-content img').addClass('lazyload');

  document.querySelectorAll('.cocoen').forEach(function(element){
    new Cocoen(element);
  });
});
